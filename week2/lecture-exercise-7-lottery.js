/*
   Create a program that generates 7 random numbers from 1 to 40. Numbers must be unique.
   Return array of lottery numbers.
   Extra: output each number with one second delay
   Hint for extra: get to know setTimeout() OR setInterval()
 */

/**
 * Return a random number withing given range.
 *   
 * @param {number} min
 * @param {number} max
 * @returns {number} random number within given range
 */
function getRandomNumber(min, max){
    const randomNumber = Math.floor(Math.random() * (max - min +1)) + min;
    return randomNumber;
}

/**
 * Function to create array of given length and fill it with unique random numbers
 * between the given range
 *
 * @param {number} n - size of the array
 * @param {number}[min=1] - minimum number for the random generated number. Default 1
 * @param {number}[max=15] - maximum number for the random generated number Default 15
 * @returns {Array.<number>} array of given length filled with unique random numbers
 */
function createRandomNumberArrayWithUniqueNumbersOnly(n, min = 1, max = 15){
    const array = [];
    let drawnNumbers = 0;
    while(drawnNumbers < n){
        const drawnNum = getRandomNumber(min, max);
        // Should use Array.find() or Array.findIndex() if processing array of objects
        if (array.indexOf(drawnNum) === -1){
            array.push(drawnNum);
            drawnNumbers++;
        }
    }
    return array;
}

/**
 * Function to print array items with given delay
 *
 * @param {Array.<any>} array
 * @param {number} delay - time in ms to delay printing
 * @param {number}[index=0] index of the array to print. Default 0
*/
// eslint-disable-next-line no-unused-vars
function delayedPrintArray(array, delay, index = 0){
    setTimeout(() => {
        if (index < array.length){
            console.log(array[index]);
            index++;
            delayedPrintArray(array, delay, index);
        }
    }, delay);
}

/**
 * Function to print array items with given delay
 *
 * @param {Array.<any>} array
 * @param {number} delay - time in ms to delay printing
 */
function delayedPrintArrayWithInterval(array, delay){
    let index = 0;
    const interval = setInterval(() => {
        if (index < array.length){
            console.log(array[index]);
            index++;
        } else {
            clearInterval(interval);
        }
    }, delay);
}

const numbers = 7;
const min = 1;
const max = 40;
const lotteryWinningNumbers = createRandomNumberArrayWithUniqueNumbersOnly(numbers, min, max);
const lotteryWinningNumbersSorted = lotteryWinningNumbers.sort((a,b) => a - b );
console.log(`The lottery winning numbers are ${lotteryWinningNumbersSorted.join(", ")}`);

// No min and max given, using function declared defaults
const vikingLotteryNumbers = createRandomNumberArrayWithUniqueNumbersOnly(5);
const vikingLotteryNumbersSorted = vikingLotteryNumbers.sort((a,b) => a - b );
console.log(`Viking lottery winning numbers are ${vikingLotteryNumbersSorted.join(", ")}`);

console.log("The ultimate Javascript jackpot winning numbers are ...");
const jackpotWinningNumbers = createRandomNumberArrayWithUniqueNumbersOnly(numbers, min, max);
const jackpotWinningNumbersSorted = jackpotWinningNumbers.sort((a,b) => a - b );
//delayedPrintArray(jackpotWinningNumbers, 1000);
delayedPrintArrayWithInterval(jackpotWinningNumbersSorted, 1000);