/**
 * Function to capitalize first letter of all the words in the sentence.
 *
 * @param {string} sentence - sentence to process
 * @returns {string} capitalized sentence
*/
function toUpperCase(sentence){
    const words = sentence.split(" ");
    for (let i = 0; i < words.length;i++){
        const word = words[i];
        words[i] = word[0].toUpperCase() + word.substring(1);
    }
    return words.join(" ");
}
const message = "this is a test word. Hello world!";
console.log(toUpperCase(message));
