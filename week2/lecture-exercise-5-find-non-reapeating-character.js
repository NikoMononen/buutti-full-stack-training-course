/*
   Exercise 5: find non-repeating character
   Find the first non-repeating character from a string.
   For example, in “aabbooooofffkkccjdddTTT” it would be “j”.
   Hint: use array method split to transform the string into array.
 */

/**
 * Function to find first non repeating character in given string.
 * Returns null if there is not a non repeating character.
 *
 * @param {string} string
 * @returns {string} character
 */
function findFirstNonRepeatingCharacter(string){
    const stringArray = string.split("");
    for (let i = 0; i < string.length; i++) {
        //const character = stringArray[i];
        //if (stringArray.filter(char => char === character).length === 1) return character;
        return stringArray.find((char, index) => char !== stringArray[index - 1] && char !== stringArray[index + 1]);
    }
    return null;
}

const string = "aabbooooofffkkccjdddTTT";
const nonRepeatingCharacter = findFirstNonRepeatingCharacter(string);

console.log(`First non repeating character in "${string}" is ${nonRepeatingCharacter}`);