
import readline from "readline";

// Globals
const letterSeparator = " ";
const letterPlaceHolder = "_";
const maxGuesses = 7;

/** 
 * Class representing a word in hangman game. 
 */
class HangmanWord {
    #word;
    #correctLetters = [];
    #guessedLetters = [];
    #wrongGuesses = 0;
    #maxGuesses = maxGuesses;
    #letterPlaceHolder = letterPlaceHolder;
    /**
     * Hangman word constructor
     * @param {string} word
    */
    constructor(word){
        this.#word = word.toLowerCase().split("");
        this.#correctLetters = [];
        this.#guessedLetters = [];
        this.#wrongGuesses = 0;
        this.#maxGuesses = maxGuesses;
        this.#letterPlaceHolder = letterPlaceHolder;
    } 
    /**
     * Returns guessed letters
     */
    getGuessedLetters = function(){
        return this.#guessedLetters;
    };
    /**
     * Returns amount of max guesses
     */
    getMaxGuesses = function(){
        return this.#maxGuesses;
    };
    /**
     * Returns amount of wrong guesses
     */
    getWrongGuesses = function(){
        return this.#wrongGuesses;
    };
    /**
     * Guess a character
     * 
     * @param {string} letter
     * @returns {boolean} if word contains character
     */
    guessLetter = function(letter){
        const letterLowerCase = letter.toLowerCase();
        // Max Guesses reached
        if(this.checkGameOver()) return false;
        if(this.#guessedLetters.includes(letterLowerCase)) {
            this.#wrongGuesses += 1;
            return false;
        }
        this.#guessedLetters.push(letterLowerCase);
        // Letter is in the word
        if (this.#word.includes(letterLowerCase)) {
            this.#correctLetters.push(letterLowerCase);
            return true;
        } else {
            this.#wrongGuesses += 1;
            return false;
        }
    };
    /**
     * Check if all the letters in the word is guessed
     * 
     * @returns {boolean} If all letters in the word is already guessed
     */
    checkResolved = function(){
        for(let i = 0; i < this.#word.length; i++){
            if (!this.#correctLetters.includes(this.#word[i])) return false;
        }
        return true;
    };
    /**
     * Check if all the letters in the word is guessed
     * 
     * @returns {boolean} If all letters in the word is already guessed
     */
    checkGameOver = function(){
        return (this.#wrongGuesses >= this.#maxGuesses);
    };
    /**
     * Returns the word with guessed characters
     * 
     * @returns {Array.<string>} Array of the word letters. Letter if it has been guessed and placeholder if not
     */
    getWord = function(){
        const lettersToShow = this.#word.map(letter => {
            if(this.#correctLetters.includes(letter)) return letter;
            else return this.#letterPlaceHolder;
        });
        return lettersToShow;
    };
}
/**
 * Loop for the game. Prompts to guess a letter, give up and move to next word or quit
 * @param {Array<HangmanWord} words - Words to play
 * @param {number}[index=0] index - Index of the current word played
 */
function promptUser(words, index = 0){
    // Node line reader settings
    const consoleLineReader = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    // Print out the word place holder
    const word = words[index];
    console.log(word.getWord().join(letterSeparator));
    /**
     * Game view after any input
     */
    const showGameView = function(){
        console.log(`☠ [${word.getWrongGuesses()}/${word.getMaxGuesses()}] Guessed letters: ${word.getGuessedLetters()}`);
        console.log(word.getWord().join(letterSeparator));
    };
    /**
     * Move to next word if there is any left
     */
    const checkNextWord = function(){
        consoleLineReader.close();
        if (index < words.length - 1){
            console.log("Next word ...");
            promptUser(words, index + 1);
        }
    };
    // Read user input
    consoleLineReader.on("line", (line) => {
        const letter = line.trim();
        switch(letter){
            case "quit": {
                console.log("Thanks for playing!");
                consoleLineReader.close();
                return;
            }
            case "give up": {
                console.log("Giving up...");
                checkNextWord();
                break;
            }
            default: {
                if (letter.length == 1) {
                    // Check the letter and print out the word
                    word.guessLetter(letter);
                    showGameView();
                    // Check if word has been resolved
                    if (word.checkResolved()){
                        console.log("You guessed the Word!!!");
                        checkNextWord();
                    } else if (word.checkGameOver()){
                        // If not resolved after guess and max wrong answer is reached
                        // hang the man
                        console.log("You have been hung!");
                        checkNextWord();
                    }
                    break;
                } else {
                    // Invalid input show game view
                    console.log(`Invalid input: ${letter}`);
                    showGameView();
                    break;
                }
            }
        }
    }); 
}
/**
 * Prints out instructions and starts the game
 * @param {Array.<HangmanWord>} words - Words to play
 */
function startGame(words){
    console.log("You can guess a letter by typing <letter> + <enter>. Quit by typing 'quit' + <enter>" + 
    "or give up on the current word by typing 'give up' + <enter>.");
    console.log("Lets play!");
    promptUser(words);
}

/*
const words = [
    new HangmanWord("AbbaACDC"),
    new HangmanWord("Pyyhkäisyelektronimikroskooppi"),
    new HangmanWord("Elintarviketurvallisuusvirasto"),
    new HangmanWord("Lentokonesuihkuturbiinimoottoriapumekaanikkoaliupseerioppilas"),
    new HangmanWord("Epäjärjestelmällistyttämättömyydellänsäkään")
];
*/
// Ready to rumble!!!
//startGame(words);

/*
    Extra. Read n amount of random words from list
*/
// Read words from the kotus XML file
import xml2js from "xml2js";
import fs from "fs";

/**
 * Return a random number withing given range.
 *  
 * @param {number} min
 * @param {number} max
 * @returns {number} random number within given range
 */
function getRandomNumber(min, max){
    const randomNumber = Math.floor(Math.random() * (max - min +1)) + min;
    return randomNumber;
}

// Get number of words from the command line
const numberOfWords = parseInt(process.argv[2]);
if (numberOfWords > 0) {
    // Parse Kotus word list and pick randomly given amount of words to play with
    const parser = new xml2js.Parser({ attrkey: "ATTR" });
    /*
    Format:
    <kotus-sanalista>
        <st><s>aakkonen</s><t><tn>38</tn></t></st>
        <st><s>aakkosellisuus</s><t><tn>40</tn></t></st>
        <st><s>aakkosittain</s><t><tn>99</tn></t></st>
        <st><s>aakkosjärjestys</s></st>
    </kotus-sanalista>
    */
    const xml_string = fs.readFileSync("./kotus-sanalista_v1.xml", "utf8");
    parser.parseString(xml_string, function(error, xmlData) {
        if(error) console.log(error);
        const words = [];
        const wordsInKotusXML = xmlData["kotus-sanalista"]["st"];
        while(words.length < numberOfWords){
            const randomNumber = getRandomNumber(0, wordsInKotusXML.length);
            const word = wordsInKotusXML[randomNumber]["s"].shift(); // Get the word from array ["<word>"]
            words.push(new HangmanWord(word));
        }
        // Ready to rumble!!!
        startGame(words);
    });
} else {
    console.log("Give amount of words to play as parameter.");
}