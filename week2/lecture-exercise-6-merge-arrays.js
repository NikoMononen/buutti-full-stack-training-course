/* 
   Create a program that joins two arrays together. If the same value appears in
   both arrays, the function should return that value only once.
   [1, 2, 3, 4]
   [3, 4, 5, 6]
   return [1, 2, 3, 4, 5, 6]
   Use different ways to join the arrays: Get to know Array.concat() method and
  spread syntax
 */

/**
 * Function to merge 2 given arrays with Array.concat() and filter out dublicates 
 *
 * @param {Array.<number>} firstArray
 * @param {Array.<number>} firstArray
 * @returns {Array.<number>} merged array with only unique items
*/
function mergeArraysUniqueWithConcat(firstArray, secondArray){
    const mergedArray = firstArray.concat(secondArray); 
    // Filter dublicates
    return filterUnique(mergedArray);
}

/**
 * Function to merge 2 given arrays using spread and filter out dublicates 
 *
 * @param {Array.<number>} firstArray
 * @param {Array.<number>} firstArray
 * @returns {Array.<number>} merged array with only unique items
*/
function mergeArraysUniqueWithSpread(firstArray, secondArray){
    const mergedArray = [...firstArray, ...secondArray];
    return filterUnique(mergedArray);
}

/**
 * Function to filter out dublicates in the given array
 *
 * @param {Array.<number>} array
 * @returns {Array.<number>} array with only unique items
 */
function filterUnique(array){
    return array.filter((number, position) => array.indexOf(number) === position);
}
const firstArray = [1, 2, 3, 4, 3];
const secondArray = [3, 4, 5, 6];
// Merged array with concat
console.log(mergeArraysUniqueWithConcat(firstArray, secondArray));
// Merged array with spread
console.log(mergeArraysUniqueWithSpread(firstArray, secondArray));