/**
    6. Generate username and password
    Create a function (or multiple functions) that generates username and password from given firstname and lastname.
    
    Username: B + last 2 numbers from current year + 2 first letters from both last name and first name in lower case
    Password: 1 random letter + first letter of first name in lowercase + last letter of last name in uppercase 
    + random special character + last 2 numbers from current year

    Example: John Doe -> B20dojo, mjE(20
    generateCredentials("John", "Doe")

    Get to know to ASCII table. Random letters and special characters can be searched from ASCII table with String.fromCharCode()
    method with indexes. For example String.fromCharCode(65) returns letter A.

    Hints: Generate random numbers (indexes) to get one random letter and one special character.
    Use range of 65 to 90 to get the LETTER and 33 to 47 to get the SPECIAL CHARACTER. 
    Use build-in function to get the current year
    Output: “username: username, password: password”
    @see {@link http://www.asciitable.com/}
 */
const characterRangeMin = 65;
const characterRangeMax = 90;
const specialCharacterRangeMin = 33;
const specialCharacterRangeMax = 47;

/**
 * Return a random number withing given range.
 *  
 * @param {number} min
 * @param {number} max
 * @returns {number} random number within given range
 */
function getRandomNumber(min, max){
    const randomNumber = Math.floor(Math.random() * (max - min +1)) + min;
    return randomNumber;
}
/**
 * Generates credentials for the user
 * 
 * @param {string} firstname
 * @param {string} lastname
 * @returns {Object} Object that contains username and password
 */
function generateCredentials(firstname, lastname){
    const username = [
        // Starts with B
        "B",
        // Year as two last digits
        new Date().getFullYear().toString().slice(-2),
        // First 2 letters of the lastname as lowercase 
        lastname.slice(0, 2).toLowerCase(),
        // First 2 letters of the firstnam es lowercase
        firstname.slice(0, 2).toLowerCase()
    ].join("");
    const password = [
        // Random character
        String.fromCharCode(getRandomNumber(characterRangeMin, characterRangeMax)),
        // First letter of firstname as lowercase
        firstname.slice(0, 1).toLowerCase(),
        // Last letter of lastname as uppercase
        lastname.slice(-1).toUpperCase(),
        // Random special character
        String.fromCharCode(getRandomNumber(specialCharacterRangeMin, specialCharacterRangeMax)),
        // Year as two last digits
        new Date().getFullYear().toString().slice(-2),
    ].join("");
    return {
        username: username,
        password: password
    };
}
console.log(generateCredentials("John", "Doe")); // John Doe -> B20dojo, mjE(20