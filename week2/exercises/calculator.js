/**
    1. Calculator
    Create a function that takes in an operator and two numbers and returns the result.
    function calculator(operator, num1, num2) {
        // your code
    };
    calculator("+", 2, 3);
    For example, if the operator is "+", sum the given numbers. In addition to sum, calculator can also calculate 
        - differences
        - multiplications
        - divisions. 
    If the operator is something else, return some error message like "Can't do that!"
*/

/**
 * Simple calculator with predefined operators
 * 
 * @param  {string} operator
 * @param  {number} a
 * @param  {number} b
 * @returns {number|string} If operator is supported, returns the operation applied to a and b. Else returns error message
 */
function calculator(operator, a, b){
    switch(operator){
        case "+": {
            return a + b;
        }
        case "-": {
            return a - b;
        }
        case "*": {
            return a * b;
        }
        case "/": {
            return a / b;
        }
        default: {
            return "Can't do that!";
        }
    }
}
// Main
const operator = process.argv[2];
const firstArgument = parseInt(process.argv[3]);
const secondArgument = parseInt(process.argv[4]);
if(operator !== undefined && operator.length === 1 && !isNaN(firstArgument) && !isNaN(secondArgument)){
    console.log(calculator(operator, firstArgument, secondArgument));
}