/*
   isPalindrome
   Check if given string is a palindrome.
   Examples:
   node .\checkPalindrome.js saippuakivikauppias -> Yes, 'saippuakivikauppias' is a palindrome
   node .\checkPalindrome.js saippuakäpykauppias -> No, 'saippuakäpykauppias' is not a palindrome
 */

/**
 * Reverses characters in the given word
 *
 * @param {string} word
 * @returns {string}
 */
function reverseWord(word){
    return word.split("").reverse().join("");
}

/**
 * Checks if the given word is palindrome
 *
 * @param {string} word
 * @returns {boolean}
 */
function isPalindrome(word){
    const wordLowerCase = word.toLowerCase();
    return wordLowerCase.toLowerCase() == reverseWord(wordLowerCase);
}

const commandLineArgument = process.argv[2];
if (commandLineArgument !== undefined && commandLineArgument.length > 0) {
    if(isPalindrome(commandLineArgument)) console.log(`Yes, "${commandLineArgument}" is not a palindrome.`);
    else console.log(`No, "${commandLineArgument}" is not a palindrome.`);
}