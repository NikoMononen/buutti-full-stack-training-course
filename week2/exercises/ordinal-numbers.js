/**
 * Ordinal numbers
 * You have two arrays:
 * const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"];
 * const ordinals = ['st', 'nd', 'rd', 'th'];
 *   
 * Create program that outputs competitors placements with following way:
 * ['1st competitor was Julia', '2nd competitor was Mark', '3rd competitor was Spencer', '4th competitor was Ann', 
 * '5th competitor was John', '6th competitor was Joe']
 */

const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

/**
 * Gets ordinal for the given number
 * 
 * Rules:
 * -st is used with numbers ending in 1 (e.g. 1st, pronounced first)
 * -nd is used with numbers ending in 2 (e.g. 92nd, pronounced ninety-second)
 * -rd is used with numbers ending in 3 (e.g. 33rd, pronounced thirty-third)
 * -th is used for all other numbers (e.g. 9th, pronounced ninth).
 * 
 * As an exception to the above rules, all the "teen" numbers ending with 11, 12 or 13 use -th 
 * (e.g. 11th, pronounced eleventh, 112th, pronounced one hundred [and] twelfth)
 * 
 * @param {number} n 
 * @returns {string} ordinal for the given number
 */
function ordinal(n){
    const remainderHundred = n % 100;
    const remainderTen = n % 10;
    if(remainderTen === 1 && remainderHundred !== 11) return ordinals[0];
    else if(remainderTen === 2 && remainderHundred !== 12) return ordinals[1];
    else if(remainderTen == 3 && remainderHundred !== 13) return ordinals[2];
    else return ordinals[3];
}

const array = [];
for (let i = 1; i < competitors.length; i++){
    array.push(`${i}${ordinal(i)} competitor was ${competitors[i - 1]}`);
}
array.forEach(element => {
    console.log(element);
});