/*
  Index of alphabets
  const charIndex = { a : 1, b : 2, c : 3, d : 4, e : 5, ... , y : 25, z : 26 };
  Create a program that turns any given word into charIndex version of the word
  Example:
  node .\charIndex.js "bead" -> 2514
  node .\charIndex.js "rose" -> 1815195
 */
const characters = "abcdefghijklmnopqrstuvwxyzåäö";
// Create object from characters and give them index number
const charIndex = {};
for (let i = 0; i < characters.length; i++){
    charIndex[characters[i]] = i + 1;
}

/**
 * Transforms given word to string of character index @see charIndex
 *
 * @param {string} word
 * @returns {string} String of characterIndexes of the given word
 */
function transformWordAsCharIndex(word){
    const array = [];
    for (let i = 0; i < word.length; i++){
        array.push(charIndex[word[i].toLowerCase()]);
    }
    return array.join("");
}

/**
 * Transforms given word to string of character index @see charIndex
 *
 * @param {string} word
 * @returns {string} String of characterIndexes of the given word
 */
function transformWordAsCharIndexWithMap(word){
    return word.split("").map(char => {
        return charIndex[char.toLocaleLowerCase()];
    }).join("");
}

const commandLineArgument = process.argv[2];
if (commandLineArgument !== undefined && commandLineArgument.length > 0) {
    console.log(transformWordAsCharIndex(commandLineArgument));
    console.log(transformWordAsCharIndexWithMap(commandLineArgument));
}