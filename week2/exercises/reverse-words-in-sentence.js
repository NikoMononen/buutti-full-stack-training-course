/*
   Reverse words in sentence
   Create a programs that reverses each word in a string.
   node .\reverseWords.js "this is a very long sentence" -> sihT si a yrev gnol ecnetnes
 */

/*
    Reverses characters in the given word using for loop

    @param {string} word
    @returns {string}
*/
// eslint-disable-next-line no-unused-vars
function reverseWordLoop(word){
    const charArray = word.split("");
    const charArrayReversed = [];
    for (let i = charArray.length - 1; i >= 0; i--){
        charArrayReversed.push(charArray[i]);
    }
    return charArrayReversed.join("");
}

/**
 * Reverses characters in the given word
 *
 * @param {string} word
 * @returns {string}
 */
function reverseWord(word){
    return word.split("").reverse().join("");
}

/**
 * Reverses all words in the given sentence
 *
 * @param {string} sentence
 * @returns {string}
 */
function reverseWordsInSentence(sentence){
    const wordsArray = sentence.split(" ");
    for (let i = 0; i < wordsArray.length; i++){
        wordsArray[i] = reverseWord(wordsArray[i]);
    }
    return wordsArray.join(" ");

}

const commandLineArgument = process.argv[2];
if (commandLineArgument !== undefined && commandLineArgument.length > 0) {
    console.log(reverseWordsInSentence(commandLineArgument));
}