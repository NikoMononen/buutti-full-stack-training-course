/**
    5. Check the exam

    The first input array is the key to the correct answers to an exam, like ["a", "a", "b", "d"] . 
    The second one contains a student's submitted answers.

    The two arrays are not empty and are the same length. Return the score for this array of answers, 
    giving +4 for each correct answer, -1 for each incorrect answer, and +0 for each blank answer, represented as an empty string.

    If the score < 0, return 0
    For example:
    checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"]) → 6 
    checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""]) → 7 
    checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"]) → 16 
    checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"]) → 0 
*/

/**
 * Checks the answers and returns score. +4 for correct answer, -1 for wrong and +-0 if empty.
 * 
 * @param {Array.<string>} correctAnswers
 * @param {Array.<string>} studentAnswers
 * @returns {number} score of the answers. If arrays are not same length or are empty -1 is returned
 */
function checkExam(correctAnswers, studentAnswers){
    if (correctAnswers.length !== studentAnswers.length || (correctAnswers.length === 0 || studentAnswers.length === 0 )){
        return -1;
    }
    return studentAnswers.reduce((score, answer, questionIndex) => {
        switch (answer){
            case correctAnswers[questionIndex]: {
                score += 4;
                break;
            }
            case "": {
                break;
            }
            default: {
                if (score > 0) score -= 1;
                break;
            }
        }
        return score;
    }, 0);
}

console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"])); // → 6 
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""])); // → 7 
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"])); // → 16 
console.log(checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"])); // → 0 