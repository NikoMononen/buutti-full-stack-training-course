/*
    Exercise 1: read/write file
    1. Implement a program that can read your given file, and print out all the information in it.
    2. Print out word for word, and every time the word you’re trying to print is “joulu”, you replace
    that word with “kinkku”, and every time your printing “lapsilla” you print “poroilla”.
    3. Finally replace the old content of the file with your new content.
    For this exercise, you can create a textFile.txt, with this content:
    Joulu on taas, joulu on taas, kattilat täynnä puuroo. Nyt sitä saa, nyt sitä saa vatsansa täyteen puuroo.
    Joulu on taas, joulu on taas, voi, kuinka meill on hauskaa! Lapsilla on, lapsilla on aamusta iltaan hauskaa.
*/
import fs from "fs";
import readline from "node:readline";

/* 
    1. Implement a program that can read your given file, and print out all the information in it. 
 */
const readStream = fs.createReadStream("./textFile.txt", "utf-8");
readStream.on("data", (txt) => {
    console.log(txt);
});

/*
  2. Print out word for word, and every time the word you’re trying to print is “joulu”, you replace
    that word with “kinkku”, and every time your printing “lapsilla” you print “poroilla”.
 */

const readStream2 = fs.createReadStream("./textFile.txt", "utf-8");
const replaceWordsObject = {
    "joulu": "kinkku",
    "lapsilla": "poroilla"
};
const readLine2 = readline.createInterface({
    input: readStream2,
    crlfDelay: Infinity
});
// Note: we use the crlfDelay option to recognize all instances of CR LF
// ('\r\n') in input.txt as a single line break.
for await (const line of readLine2) {
    const words = line.split(" ");
    const mappedWords = words.map(word => {
        const wordLowerCase = word.toLocaleLowerCase();
        if(wordLowerCase in replaceWordsObject){
            const firstLetter = word[0];
            // First letter is uppercase
            word = replaceWordsObject[wordLowerCase];
            if (firstLetter.toUpperCase() === firstLetter){
                word = word[0].toUpperCase() + word.slice(1);
            } 
        }
        return word;
    });
    console.log(mappedWords.join(" "));
}

// Same with reading the whole file in to the memory
fs.readFile("./textFile.txt", "utf-8", (err, data) => {
    if (err) console.log(err);
    else {
        const rows = data.split("\n");
        const newRows = [];
        rows.forEach(row => {
            const words = row.split(" ");
            const mappedWords = words.map(word => {
                const wordLowerCase = word.toLocaleLowerCase();
                if(wordLowerCase in replaceWordsObject){
                    const firstLetter = word[0];
                    // First letter is uppercase
                    word = replaceWordsObject[wordLowerCase];
                    if (firstLetter.toUpperCase() === firstLetter){
                        word = word[0].toUpperCase() + word.slice(1);
                    } 
                }
                return word;
            });
            newRows.push(mappedWords.join(" "));
        });
        console.log(newRows.join("\n"));
    } 
});

/*
    3. Finally replace the old content of the file with your new content.
 */

/**
 * Replaces given words in the file and writes new content to given output file
 * @param  {string} inputFile - input filename with path
 * @param  {string} outputFile - output filename with path
 * @param  {Object} wordsToReplaceObject - Object of words to replace in format of "{word}": "{replaced}"
 */
function replaceWordsInFile(inputFile, outputFile, wordsToReplaceObject){
    const writeStream = fs.createWriteStream(outputFile);
    const readStream3 = fs.createReadStream(inputFile, "utf-8");
    const readLine = readline.createInterface({
        input: readStream3,
        crlfDelay: Infinity
    });
    readLine.on("line", function(line) {
        const words = line.split(" ");
        const mappedWords = words.map(word => {
            const wordLowerCase = word.toLocaleLowerCase();
            if(wordLowerCase in wordsToReplaceObject){
                const firstLetter = word[0];
                // First letter is uppercase
                word = wordsToReplaceObject[wordLowerCase];
                if (firstLetter.toUpperCase() === firstLetter){
                    word = word[0].toUpperCase() + word.slice(1);
                } 
            }
            return word;
        });
        // Join words and add line ending
        const lineReplaced = mappedWords.join(" ") + "\n";
        // Write lines to the file
        writeStream.write(lineReplaced, (err) => {
            if (err) console.log(err);
        });
    });
}

// Read and replace lines in the file and write to new file
replaceWordsInFile("./textFile.txt", "./textFileModified.txt", replaceWordsObject);