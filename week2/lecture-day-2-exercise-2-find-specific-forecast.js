/*
    Exercise 2: find specific forecast
    const forecast = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
    }
    1. Save forecast to forecast_data.json
    2. Read forecast from the file and modify the temperature of that forecast.
    3. Save updated forecast back to the file.
*/
import fs from "fs";

const forecast = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};

/*
    1. Save forecast to forecast_data.json
*/
const forecastFile = "./forecast_data.json";

/**
 * Writes object to specified file as JSON
 * @param  {Object} object - JSON object
 * @param  {string} file - path and filename for the file to save object
 * @callback callback - The callback to call after write
 */
const writeJSONObjectToFile = function(object, file, callback){
    const objectAsString = JSON.stringify(object);
    fs.writeFile(file,  objectAsString, (err) => {
        if (err) console.log(err);
        else callback ? callback() : console.log(`Successfully write of ${objectAsString} to file ${file}`);
    });
};
writeJSONObjectToFile(forecast, forecastFile);

/*
    2. Read forecast from the file and modify the temperature of that forecast.
    3. Save updated forecast back to the file.
*/

/**
 * Reads specified file and returns object or calls given callback 
 * @param  {string} file - Filename with path
 * @callback callback - The callback to handle read JSON object
 * @returns {Object} Returns parsed JSON object from the file if no callback is specified
 */
const readJSONObjectToFile = function(file, callback){
    try {
        const jsonData = fs.readFile(file, "utf-8", (err, data) => {
            if (err) console.log(err);
            else {
                if(callback !== undefined) {
                    callback(JSON.parse(data));
                } 
                return JSON.parse(data);
            }
        });
        return jsonData;
    }
    catch (err){
        console.log("No data found.");
    }
};
// Read the forecast file, add 1 to temperature and write to file
readJSONObjectToFile(forecastFile, function(forecastObject){
    forecastObject.temperature +=1;
    writeJSONObjectToFile(forecastObject, forecastFile);
});

