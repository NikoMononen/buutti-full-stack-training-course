class Shape {
    constructor(width, height){
        this.width = width;
        this.height = height;
    }
    /* Override this method */
    calculateArea = function(){
        return -1;
    };
    /* Override this method */
    calculateCircumference(){
        return -1;
    }

}
class Rectangle extends Shape {
    calculateArea = function(){
        return this.width * this.height;
    };
    calculateCircumference(){
        return 2 * (this.width + this.height);
    }
}
class Circle extends Shape {
    constructor(diameter){
        super(diameter, diameter);
    }
    calculateArea = function(){
        return Math.PI * Math.pow(this.width, 2);
    };
    calculateCircumference = function(){
        return 2 * (Math.PI * (this.width / 2));
    };
}
class Ellipse extends Shape {
    calculateArea = function(){
        // Area = Pi * Width/2 * Height/2
        return Math.PI * (this.width / 2) * (this.height / 2);
    };
    calculateCircumference = function(){
        // 2 * Pi * √Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2 )
        return 2 * Math.PI * Math.sqrt((Math.pow(this.width, 2) + Math.pow(this.height, 2)) / 2);
    };
}
class RectangularTriangle extends Shape {
    calculateArea = function(){
        return (1/2) * this.width * this.height;
    };
    calculateCircumference = function(){
        const a = this.width;
        const b = this.height;
        const c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2)); // c = sqrt( a^2 + b^2 )
        return a + b + c;
    };
}
class Triangle extends Shape {
    calculateArea = function(){
        return (this.height * this.width) / 2;
    };
    calculateCircumference = function(){
        const hypotenuse = Math.sqrt(Math.pow((this.width / 2), 2)+ Math.pow((this.height / 2), 2));
        return this.width + hypotenuse * 2;
    };
} 
class TriangleWithSides extends Shape {
    constructor(sideA, sideB, sideC){
        // Not really smart to extend Shape because if any of the sides is changed
        // this.height and this.height is not correct any more
        const halfPerimiter = (sideA + sideB + sideC) / 2;
        const area = Math.sqrt(halfPerimiter * ((halfPerimiter - sideA) * (halfPerimiter - sideB) * (halfPerimiter - sideC)));
        // 2 * area / b
        const height = (2 * area) / sideA;
        super(sideA, height);
        this.a = sideA;
        this.b = sideB;
        this.c = sideC;
    }
    calculateArea = function(){
        // Heron's Formula for the area of a triangle
        const halfPerimiter = this.calculateCircumference() / 2;
        // Area	= √{p(p−a)(p−b)(p−c)}
        const area = Math.sqrt(halfPerimiter * (halfPerimiter - this.a) * (halfPerimiter - this.b) * (halfPerimiter - this.c));
        return area;
    };
    calculateCircumference = function(){
        return this.a + this.b + this.c;
    };
} 
const circle = new Circle(5);
const rectangle = new Rectangle(5, 7);
const ellipse = new Ellipse(10, 5);
const triangle = new Triangle(3, 5);
const rectangularTriangle = new RectangularTriangle(5, 7);
const triangleA = 24; 
const triangleB = 30;
const triangleC = 18;
const triangleWithSides = new TriangleWithSides(triangleA, triangleB, triangleC);
// width and height is not defined
const shapes = [circle, ellipse, rectangle, triangle, rectangularTriangle, triangleWithSides];

for(const shape of shapes) {
    console.log("Width: " + shape.width);
    console.log("Height: " + shape.height);
    console.log("Circumference: " + shape.calculateCircumference());
    console.log("Area: " + shape.calculateArea());
    console.log("---------------------");
}
console.log(`Triangle with sides A: ${triangleA}, B: ${triangleB} and C: ${triangleC}. Area is ${triangleWithSides.calculateArea()} and circumference is ${triangleWithSides.calculateCircumference()}`);