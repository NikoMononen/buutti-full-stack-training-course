
/**
 * Subtracts b from a and calls function fn
 * 
 * @param {function} fn 
 * @param {int} a 
 * @param {int} b 
 * @returns value from function
 */
export const subtractFib = (fn, a, b) => {
    return fn(a - b);
};