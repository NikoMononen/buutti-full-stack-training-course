import logo from './logo.svg';
import React, { useState } from 'react'
import './App.css';

function App() {
  const [textValue, setTextValue] = useState("");
  const [textValuesList, setTextValuesList] = useState([]);

  const submitFormAction = (event) => {
    event.preventDefault();

    textValuesList.push(textValue);
    setTextValue("");
  }

  return (
    <div className="App">
      <header className="App-header">
        <form onSubmit={submitFormAction}>
        <input type="text" value={textValue} onChange={(e) => setTextValue(e.target.value)} />
        <input type="submit" value="Submit" />
        </form>
        <ul aria-label="items">
          { 
            textValuesList.map(item => {
              return <li key={item}>{item}</li>
            })
          }
        </ul>
      </header>
    </div>
  );
}

export default App;
