import { jest } from "@jest/globals";
import { subtractFib } from "./lecture-exercise-3-Mocking.js";
import { fib } from "./lecture-exercise-2-Fibonacci-sequence.js";

describe("Mocked fibonacci subtract tests", () => {
    // Mocked fibonacci
    let mockFn;

    beforeEach(async ()  => {
        // Clear the cache
        jest.resetModules();

        // Reset mock function
        mockFn = jest.fn(n => 0 + n);
    });

    test("subtractFib(fn, 3, 2) = 1", () => {
        expect(subtractFib(mockFn, 3, 2)).toBe(1);
        // mockFn called once with 1
        expect(mockFn.mock.calls.length).toBe(1);
        expect(mockFn.mock.calls[0][0]).toBe(1);
        expect(mockFn.mock.results[0].value).toBe(1);

    });

    test("subtractFib(fn, 5, 1) = 4", () => {
        expect(subtractFib(mockFn, 5, 1)).toBe(4);
        expect(mockFn.mock.calls.length).toBe(1);
        expect(mockFn.mock.calls[0][0]).toBe(4);
        expect(mockFn.mock.results[0].value).toBe(4);       
    });

    test("subtractFib(fn, 3, 2) = 1 and subtractFib(fn, 5, 1) = 4", () => {
        // First call
        expect(subtractFib(mockFn, 3, 2)).toBe(1);
        expect(mockFn.mock.calls.length).toBe(1);
        expect(mockFn.mock.calls[0][0]).toBe(1);
        expect(mockFn.mock.results[0].value).toBe(1);

        // Second call
        expect(subtractFib(mockFn, 5, 1)).toBe(4);
        expect(mockFn.mock.calls.length).toBe(2);
        expect(mockFn.mock.calls[1][0]).toBe(4);
        expect(mockFn.mock.results[1].value).toBe(4);       
    });

    test("subtractFib(fn, 3, 2) = 1 and subtractFib(fn, 7, 1) = 8 with real fibonacci function", () => {
        // Create mock from real fibonacci function
        mockFn = jest.fn(fib);

        // First call
        expect(subtractFib(mockFn, 3, 2)).toBe(1);
        expect(mockFn.mock.calls.length).toBe(1);
        expect(mockFn.mock.calls[0][0]).toBe(1);
        expect(mockFn.mock.results[0].value).toBe(1);

        // Second call
        expect(subtractFib(mockFn, 7, 1)).toBe(8);
        expect(mockFn.mock.calls.length).toBe(2);
        expect(mockFn.mock.calls[1][0]).toBe(6);
        expect(mockFn.mock.results[1].value).toBe(8);    
    });
});
