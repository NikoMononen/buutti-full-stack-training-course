import { sum, subtract } from "./lecture-exercise-1-Jest.js";

test("adds 1 + 2 to equal 3", () => {
    expect(sum(1, 2)).toBe(3);
});

test("adds 1 - 2 to equal -1", () => {
    expect(subtract(1, 2)).toBe(-1);
});