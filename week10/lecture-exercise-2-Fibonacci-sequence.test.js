import { fib, fibonacciNumberRecursively } from "./lecture-exercise-2-Fibonacci-sequence.js";

describe("Fibonacci tests", () => {
    test("fib(-1) = TypeError: Only positive numbers are allowed", () => {
        const t = () => {
            fib(-1);
        };
        expect(t).toThrow(TypeError);
        expect(t).toThrow("Only positive numbers are allowed");
    });

    test("fib(0) = 0", () => {
        expect(fib(0)).toBe(0);
    });

    test("fib(1) = 1", () => {
        expect(fib(1)).toBe(1);
    });

    test("fib(2) = 1", () => {
        expect(fib(2)).toBe(1);
    });

    test("fib(5) = 5", () => {
        expect(fib(5)).toBe(5);
    });

    test("fib(6) = 8", () => {
        expect(fib(6)).toBe(8);
    });

    test("fibonacciNumberRecursively(0) = [0]", () => {
        expect(fibonacciNumberRecursively(0)).toStrictEqual([0]);
    });

    test("fibonacciNumberRecursively(1) = [0, 1]", () => {
        expect(fibonacciNumberRecursively(1)).toStrictEqual([0, 1]);
    });

    test("fibonacciNumberRecursively(2) = [0, 1, 1]", () => {
        expect(fibonacciNumberRecursively(2)).toStrictEqual([0, 1, 1]);
    });

    test("fibonacciNumberRecursively(5) = [0, 1, 1, 2, 3, 5]", () => {
        expect(fibonacciNumberRecursively(5)).toStrictEqual([0, 1, 1, 2, 3, 5]);
    });

    test("fibonacciNumberRecursively(6) = [0, 1, 1, 2, 3, 5, 8]", () => {
        expect(fibonacciNumberRecursively(6)).toStrictEqual([0, 1, 1, 2, 3, 5, 8]);
    });
});




