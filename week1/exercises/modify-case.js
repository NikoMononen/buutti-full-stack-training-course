/*
    Modify case
    Create a program that takes in a string, and modifies the every letter of that string to upper case or lower case, depending on the input
    example: node .\modifycase.js lower "Do you LIKE Snowmen?" -> do you like snowmen
    example: node .\modifycase.js upper "Do you LIKE Snowmen?" -> DO YOU LIKE SNOWMEN NOTE remember to take in the 2nd parameter with quotation mark
*/
/*
    Checks string is valid
*/
function isValidString(string) {
    if (typeof string === "undefined" || string.length < 1) return false;
    return true;
}
/*
    Applies given action to the message if action is defined, otherwise return message
*/
function doActionToMessage(action, message){
    switch(action){
        case "upper":
            return message.toUpperCase();
        case "lower":
            return message.toLowerCase();
        default:
            return message;
    }

}
// Read inputs from commandline
const action = process.argv[2];
const message = process.argv[3];

if (isValidString(action) && isValidString(message)) {
    const transformedMessage = doActionToMessage(action,message);
    console.log(transformedMessage);
}
