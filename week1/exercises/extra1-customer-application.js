const readline = require("readline");
const consoleLineReader = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

/*
    - jaollinen 2000 = lahjakortti
    - yli 1000 ja jaollinen 25 = ilmapallo
    - muuten ei mitään

    ---
    Original:
    z.question('Kuinka mones kävijä tänään? ', (y) => {
        if (y >= 1000 && y % 25 === 0) {
        console.log('Saa ilmapallon!')
        } else if (y % 2000 === 0) {
        console.log('Saa lahjakortin!!!')
        } else if (y % 2000 !== 0 || (y < 1000 && y % 25 !== 0)) {
        console.log('Ei saa mitään. :(')
        }
    z.close()
    });
    ---
*/
consoleLineReader.question("Kuinka mones kävijä tänään? ", (answer) => {
    const answerAsInt = parseInt(answer);
    if(isNaN(answerAsInt)){
        console.log("Kävijämäärän on oltava numero.");
        consoleLineReader.close();
        return;
    }
    if (answerAsInt % 2000 === 0) {
        console.log("Saa lahjakortin!!!");
    } else if (answerAsInt >= 1000 && answerAsInt % 25 === 0) {
        console.log("Saa ilmapallon!");
    } else {
        console.log("Ei saa mitään. :(");
    }
    consoleLineReader.close();
});