// Seconds in year
const days = 365;
const hours = 24;
const minutes = 60;
const seconds = 60;

const seconds_in_year = seconds * minutes * hours * days;
console.log(`Seconds in year: ${seconds_in_year}`);
