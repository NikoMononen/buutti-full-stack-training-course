/*
    Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.
    example: node .\replacecharacters.js g h "I have great grades for my grading" -> I have hreat hrades for my hrading
    Hint: https://www.w3schools.com/jsref/jsref_replace.asp
*/

/*
    Checks string is valid
*/
function isValidString(string) {
    if (typeof string === "undefined" || string.length < 1) return false;
    return true;
}
/*
    Applies given action to the message if action is defined, otherwise return message
*/
function replaceCharactes(characterToReplace, characterWithToReplace, message){
    return message.replaceAll(characterToReplace, characterWithToReplace);
}

// Read inputs from commandline
const message = process.argv[4];
const characterToReplace = process.argv[2];
const characterWithToReplace = process.argv[3];

if (isValidString(message) && isValidString(characterToReplace) && isValidString(characterWithToReplace)) {
    const transformedMessage = replaceCharactes(characterToReplace, characterWithToReplace, message);
    console.log(transformedMessage);
}
