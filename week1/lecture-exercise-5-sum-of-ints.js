// Sum of integers from 1 to n with given number n
function sumOfNumbersMultiplesOf3Or5WithForLoop(n){
    let sum = 0;
    for (let i = 1; i <= n; i++){
        sum += i;        
    }
    return sum;
}
function sumOfNumbersMultiplesOf3Or5WithWhileLoop(n){
    let sum = 0;
    let i = 0;
    while(i <= n){
        sum += i; 
        i++;      
    }
    return sum;
}
const numberFromCommandline = parseInt(process.argv[2]);
console.log(sumOfNumbersMultiplesOf3Or5WithForLoop(numberFromCommandline));
console.log(sumOfNumbersMultiplesOf3Or5WithWhileLoop(numberFromCommandline));