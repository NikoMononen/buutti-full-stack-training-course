/*
Print out the largest number found in this array.
● Find the largest number without first sorting the array, or without using
Math.max() function.
Level 2:
● Find the 2nd largest number in the array, without sorting the array first, or
using Math functions to aid you.
*/
const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

/*
    Finds the largest number in the array
    
    @param array array to find largest number
    @return largestNumber
*/
function findLargestNumber(array){
    let largestNumber = array[0];
    for (let i = 1; i < array.length; i++) {
        if (array[i] > largestNumber) largestNumber = array[i];
    }
    return largestNumber;
}

/*
    Finds the 2nd largest number in the array
    
    @param array array to find 2nd largest number
    @return secondLargestNumber
*/
function find2ndLargestNumber(array){
    let largestNumber = array[0];
    let secondLargestNumber = -Infinity;
    for (let i = 1; i < array.length; i++) {
        if (array[i] > largestNumber){
            secondLargestNumber = largestNumber;
            largestNumber = array[i];
        }
        else if (array[i] > secondLargestNumber && array[i] < largestNumber){
            secondLargestNumber = array[i];
        }
    }
    return secondLargestNumber;
}
console.log(findLargestNumber(arr));
//console.log(Math.max(...arr));
console.log(find2ndLargestNumber(arr));