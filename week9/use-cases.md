# App
- App is created to create simulations different bought services eg. GPS, 5G, GSM and present them visually easy to read format
- Services which users can buy within the app run on cloud platform with paid resource usage limits
- User can buy and increase available resources capacity

# User profile
- Typical user is engineer or researcher 
- They want to see lot of different kind information
- Commonly used functions should be easily found
- Customizing parameters is important
- Exporting graphs and data is important

# Use cases
### Overview of the service
As user I want to see brief overview of my project.

### Overview
As user I want to see more detailed view when I select a topic.

### Detailed view
As user I want to see detailed graph of the data I choose to be displayed.

### Detailed view 2
As user I want to see modify graph parameters and it should show in the graph.asr