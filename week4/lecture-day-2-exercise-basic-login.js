
import readline from "readline";
import bcrypt from "bcrypt";

const consoleLineReader = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

//const bcryptHashRounds = 10;
const correctPasswordHash = "$2b$10$FVRTZtXEVp6nsp52GAkFdOWYcRYI7XTRVoxOSWv7GKSkAlOdwD2Wi";

consoleLineReader.question("Give password: ", (line) => {
    bcrypt.compare(line, correctPasswordHash).then((result) => {
        if (result)
            console.log("Password is correct!");
        else 
            console.log("Password is incorrect.");
    });
    consoleLineReader.close();
});

