import express from "express";
import fs from "fs";
import uuid4 from "uuid4";

/*

POST http://localhost:5000/student
Content-Type: application/json

{
    "id": "f533a8f5-56fc-4101-9ccd-5d20e8969c92",
    "name": "Testi Testinen",
    "email": "testi.testinen@test.fi"
}

*/
const app = express();
const port = 5000;
const databaseFile = "week4/lecture-exercise-5-student-registry-db.txt";

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

let students = [];

// Save database on PUT, POST and DELETE requests
app.use(function(request, response, next) {
    if (["PUT","POST", "DELETE"].includes(request.method)) {
        response.on("finish", function() {
            fs.writeFile(databaseFile, JSON.stringify(students),(err) => {
                if (err) console.log(err);
                console.log("Students saved");
                console.log(students);
            });
        });
    }
    next();
});

app.get("/student", (request, response) => {
    response.send(students);
});

app.post("/student", (request, response) => {
    const index = students.findIndex(student => {
        return student.id === request.body.id;
    });

    if (index === -1) {
        const student = { ...request.body, id : uuid4() };
        students.push(student);
        response.send(student);
    }
    else {
        response.status(500).send({
            error: "User with given id already exists or id is missing" 
        });
    }
});

app.get("/student/:id", (request, response) => {
    const index = students.findIndex(student => {
        return student.id === request.params.id;
    });

    if (index >= 0) {
        response.send(students[index]);
    } else {
        response.status(500).send({ error: "User not found" });
    }
});

app.put("/student/:id", (request, response) => {
    const index = students.findIndex(student => {
        return student.id === request.params.id;
    });

    if (index >= 0) {
        students[index] = {...request.body, id: students[index].id };
        response.send(students[index]);
    } else {
        response.status(500).send({ error: "User not found" });
    }
});

app.delete("/student/:id", (request, response) => {
    const index = students.findIndex(student => {
        return student.id === request.params.id;
    });

    if (index >= 0) {
        students.splice(index, 1);
        response.send();
    } else {
        response.status(500).send({ error: "User not found" });
    }
});

app.listen(port, () => {
    try {
        students = JSON.parse(fs.readFileSync(databaseFile, {encoding:"utf8", flag:"r"}));
        console.log("Read from database");
    } catch(error) {
        students = [];
        console.log("Creating new database");
    }
    console.log(students);
    console.log(`Listening to port ${port}`);
});