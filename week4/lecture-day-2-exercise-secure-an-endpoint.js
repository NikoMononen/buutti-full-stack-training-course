import express from "express";
import bcrypt from "bcrypt";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";

const app = express();
const port = 5000;

dotenv.config();
const correctUsername = process.env.CORRECT_USERNAME;
const correctPasswordHash = process.env.CORRECT_PASSWORD;

// Logger middleware
const logger = (request, response, next) => {
    const fullUrl = request.protocol + "://" + request.get("host") + request.originalUrl;
    response.on("finish", () => {
        console.log(`${request.method} ${fullUrl} ${response.statusCode} ${response.statusMessage}`);
    });
    next();
};

// Authorization middleware
const authorization = (request, response, next) => {
    const authToken = request.get("authorization");
    if (authToken && authToken.startsWith("Bearer ")){
        try {
            // Substring "Bearer " and verify token
            const token = authToken.substring(7);
            const decodedToken = jwt.verify(token, process.env.APP_SECRET);

            if (!decodedToken.username) {
                return response.status(403).json({
                    error: "Access token is invalid"
                });
            } else {
                next();
            }
        }
        catch (error) {
            switch (error.constructor) {
                case jwt.TokenExpiredError: {
                    return response.status(403).json({
                        error: error.message
                    });
                }
                case jwt.JsonWebTokenError: {
                    return response.status(401).json({
                        error: error.message
                    });
                }
                default: {
                    return response.status(500).json({
                        error: error.message
                    });
                }
            }
        }
    } else {
        return response.status(401).json({
            error: "Auth token missing"
        });
    }
};

// Express configuration
app.use("/", express.static("static"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Use logging
app.use(logger);

// Routes

app.post("/login", (request, response) => {
    const username = request.body.username;
    const password = request.body.password;

    if (correctUsername === username) {
        bcrypt.compare(password, correctPasswordHash)
            .then((passwordIsCorrect) => {
                if (passwordIsCorrect) {
                    // Sign JWT token
                    const token = jwt.sign(
                        { username: username },
                        process.env.APP_SECRET,
                        { expiresIn: "1h" }
                    );
                    response.status(200).json({
                        "status": "Access granted!",
                        "token": token
                    });
                }
                else {
                    response.status(401).json({
                        "status": "Access denied!"
                    });
                }
            });
    } else {
        response.status(401).json({
            "status": "Access denied!"
        });
    }
});

app.get("/public", (request, response) => {
    response.status(200).json({
        "public": "public-value"
    });
});

// Router for secret paths that require authentication
const secretsRouter = express.Router();

// Use authorization
secretsRouter.use(authorization);

secretsRouter.get("/", (request, response) => {
    response.status(200).json({
        "secret": "secret-value"
    });
});

app.use("/secrets", secretsRouter);

app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});