import express from "express";
import bcrypt from "bcrypt";
import dotenv from "dotenv";


const app = express();
const port = 5000;

dotenv.config();
const correctUsername = process.env.CORRECT_USERNAME;
const correctPasswordHash = process.env.CORRECT_PASSWORD;

// Middleware logger
app.use("*", (request, response, next) => {
    const fullUrl = request.protocol + "://" + request.get("host") + request.originalUrl;
    console.log(`${request.method} ${fullUrl}`);
    next();
});

app.use("/", express.static("static"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.post("/login", (request, response) => {
    if (correctUsername === request.body.username) {
        bcrypt.compare(request.body.password, correctPasswordHash)
            .then((passwordIsCorrect) => {
                if (passwordIsCorrect) response.status(200).json({
                    "status": "Access granted!"
                });
                else response.status(401).json({
                    "status": "Access denied!"
                });
            });
    } else {
        response.status(401).json({
            "status": "Access denied!"
        });
    }
});

app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});