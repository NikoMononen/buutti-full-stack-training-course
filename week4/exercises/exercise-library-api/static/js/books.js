// Axios not locally installed
/* eslint-disable no-undef */

// API URL
const apiPath = "http://localhost:5000/api/v1";

// Store books data locally
const cachedBooks = [];

/**
 * Add event handlers to book element buttons
 * @param {Node} booksCollectionDiv 
 */
const addBookEventHandlers = (booksCollectionDiv) => {
    // Mark read handlers
    const markReadButtons = booksCollectionDiv.querySelectorAll("button[data-attr=read]");

    for (let i = 0; i < markReadButtons.length; i++){
        const element = markReadButtons[i];

        element.addEventListener("click", (event) => {
            const id = event.target.parentElement.attributes["data-attr-id"].value;
            // Update book and set read = true
            const bookIndex = cachedBooks.findIndex((book) => book.id === id);
            const formData = cachedBooks[bookIndex];
            formData.read = "true";
            axios.put(apiPath + `/books/${id}`, formData, {
                headers: { "Content-Type": "application/json" },
            }).then(function (response) {
                console.log(response);
                cachedBooks[bookIndex].read = true;
                templateBooks();
            }).catch(function (response) {
                console.log(response);
                //TODO show error message
            });
            event.preventDefault();
        });
    }

    // Delete book handlers
    const deleteButtons = booksCollectionDiv.querySelectorAll("button[data-attr=delete]");

    for (let i = 0; i < deleteButtons.length; i++){
        const element = deleteButtons[i];

        element.addEventListener("click", (event) => {
            const deleteBookId = event.target.parentElement.attributes["data-attr-id"].value;
            const deleteBookIndex = cachedBooks.findIndex((book) => book.id === deleteBookId);
            if (deleteBookIndex >= 0  && confirm("Do you really want to delete book?")) {
                axios.delete(apiPath + `/books/${deleteBookId}`, {
                    headers: { "Content-Type": "application/json" },
                }).then(function (response) {
                    console.log(response);
                    cachedBooks.splice(deleteBookIndex, 1);
                    templateBooks();
                }).catch(function (response) {
                    console.log(response);
                    //TODO show error message
                });
            }
            event.preventDefault();
        });
    }

    // Edit book handlers
    const editButtons = booksCollectionDiv.querySelectorAll("button[data-attr=edit]");

    for (let i = 0; i < editButtons.length; i++){
        const element = editButtons[i];

        element.addEventListener("click", (event) => {
            const editBookId = event.target.parentElement.attributes["data-attr-id"].value;
            const editBookIndex = cachedBooks.findIndex((book) => book.id === editBookId);
            const editBook = cachedBooks[editBookIndex];

            const addBookForm = document.getElementById("book-form");
            addBookForm.elements.id.value = editBook.id;
            addBookForm.elements.name.value = editBook.name;
            addBookForm.elements.author.value = editBook.author;
            addBookForm.elements.read.value = editBook.read;
            
            // Show book panel
            document.getElementById("panel-default").classList.add("hidden");
            document.getElementById("panel-book").classList.remove("hidden");
            event.preventDefault();
        });
    }
};

/**
 * Clear book form
 */
const clearBookForm = () => {
    const form = document.getElementById("book-form");
    form.elements.id.value = null;
    form.elements.name.value = null;
    form.elements.author.value = null;
    form.elements.read.value = null;
    form.elements.readYes.checked = false;
    form.elements.readNo.checked = true;
};

/*
 *   Template for book element
 */
const templateBooks = () => {
    const booksCollectionDiv = document.getElementById("books-collection");
    
    const rows = cachedBooks.map(book => {
        const bookReadImage = book.read ? "<span><img src=\"img/check-mark.svg\" class=\"book-read-img\"></span>" : "";
        return `
    <div id="${book.id}" class="col">
        <div class="card shadow-sm">
            <div class="card-body${ book.read ? " book-read" : "" }">
                <h3>${book.name}${bookReadImage}</h2>
                <p class="card-text">${book.author}</p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group" data-attr-id="${book.id}">
                        <button type="button" data-attr="read" ${ book.read ? "disabled=\"disabled\"" : "" } class="btn btn-sm btn-outline-secondary">Mark read</button>
                        <button type="button" data-attr="edit"class="btn btn-sm btn-outline-secondary">Edit</button>
                        <button type="button" data-attr="delete" class="btn btn-sm btn-secondary">Delete</button>
                    </div>
                </div>
                <div class="text-muted book-id"><small class="text-muted">${book.id}</small></div>
            </div>
        </div>
    </div>`;
    });

    booksCollectionDiv.innerHTML = rows.join("");
    addBookEventHandlers(booksCollectionDiv);
};

/*
 *   Get all books from the database
 */
const getAllBooks = () => {
    axios.get(apiPath + "/books",)
        .then((response) => {
            const newBooks = response.data.reverse();
            if (newBooks.length > 0) {
                // Empty cache
                cachedBooks.splice(0,cachedBooks.length);
                newBooks.forEach((book) => {
                    cachedBooks.push(book);
                });
                templateBooks();
            } else {
                booksCollectionDiv.innerHTML = "<p>There are no books in the collection.</p>";
            }
        }).catch((error) => {
            console.log(error);
        });
};

/*
 *   On load actions
 */
window.onload = () => {
    // Events
    const addBookButton = document.getElementById("add-book-link");
    addBookButton.addEventListener("click", (event) => {
        document.getElementById("panel-default").classList.add("hidden");
        document.getElementById("panel-book").classList.remove("hidden");
        event.preventDefault();
    });

    const cancelAddBookButton = document.getElementById("cancel-add-book-link");
    cancelAddBookButton.addEventListener("click", (event) => {
        clearBookForm();
        document.getElementById("panel-default").classList.remove("hidden");
        document.getElementById("panel-book").classList.add("hidden");
        event.preventDefault();
    });
    
    const addBookForm = document.getElementById("book-form");
    addBookForm.addEventListener("submit", (event) => {
        const form = event.target;
        let formMethod = "POST";
        let formPath = "/books";
        const formData = {
            "name": form.elements.name.value,
            "author": form.elements.author.value,
            "read": form.elements.read.value
        };

        // If id is defined, we update book instead of adding new one
        if (form.elements.id.value !== undefined && form.elements.id.value !== "" && form.elements.id.value !== null ) {
            formData["id"] = form.elements.id.value;
            formMethod = "PUT";
            formPath = formPath + "/" + formData["id"];
        }

        axios(apiPath + formPath, {
            method: formMethod,
            data: formData,
            headers: { "Content-Type": "application/json" },
        }).then(function (response) {
            console.log(response);
            clearBookForm();
            // Hide book panel
            document.getElementById("panel-default").classList.remove("hidden");
            document.getElementById("panel-book").classList.add("hidden");
            getAllBooks();
        }).catch(function (response) {
            console.log(response);
        });
        event.preventDefault();
    });

    // Load all books from the database
    getAllBooks();
};