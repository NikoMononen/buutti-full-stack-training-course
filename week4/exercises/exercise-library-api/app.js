import express from "express";
import cors from "cors";
import router from "./routes/books.route.js";
import { saveDatabase, readDatabase } from "./controllers/books.controller.js";

const app = express();
const port = 5000;

// eslint-disable-next-line no-unused-vars
export const databaseFile = "db.json";
const apiPrefix = "/api/v1";


// Middleware logger
app.use("*", (request, response, next) => {
    const fullUrl = request.protocol + "://" + request.get("host") + request.originalUrl;
    console.log(`${request.method} ${fullUrl}`);
    next();
});

app.use("/", express.static("static"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Use books router
app.use(apiPrefix + "/books", router);

// Save database on PUT, POST and DELETE requests
app.use(function(request, response, next) {
    if (["PUT","POST", "DELETE"].includes(request.method)) {
        response.on("finish", function() {
            saveDatabase();
        });
    }
    next();
});

// Start app
app.listen(port, () => {
    readDatabase();
    console.log(`Listening to port ${port}`);
});