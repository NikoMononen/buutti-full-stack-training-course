import express from "express";
import * as BooksController from "../controllers/books.controller.js";

const router = express.Router();

router.get("/", BooksController.getAllBooks);
router.get("/:id", BooksController.getBook);
router.post("/", BooksController.postBook);
router.put("/:id", BooksController.putBook);
router.delete("/:id", BooksController.deleteBook);

export default router;