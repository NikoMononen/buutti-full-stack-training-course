import express from "express";
import cors from "cors";
import helmet from "helmet";
import fs from "fs";
import booksRouter from "./routes/books.route.js";
import usersRouter from "./routes/users.route.js";
import authenticationRouter from "./routes/authentication.route.js";
import { users } from "./controllers/users.controller.js";
import { books } from "./controllers/books.controller.js";
import dotenv from "dotenv";

dotenv.config();

export const app = express();
const port = 5000;

const databaseFile = process.env.DATABASE_JSON;
const apiPrefix = process.env.API_PREFIX;

/**
 * Save database to JSON file
 */
export const saveDatabase = () =>{
    const db = {
        users: users,
        books: books
    };
    fs.writeFile(databaseFile, JSON.stringify(db),(err) => {
        if (err) console.info(err);
    });
};

/**
 * Read database from JSON file to cached variable
 */
export const readDatabase = () => {
    try {
        const db = JSON.parse(fs.readFileSync(databaseFile, {encoding:"utf8", flag:"r"}));
        db.users.forEach(user => users.push(user));
        db.books.forEach(book => books.push(book));
        console.info("Read from database");
    } catch(error) {
        console.info("Creating new database");
    }
    console.info({
        users: users,
        books: books
    });
};

// Middleware
app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Logger middleware
const logger = (request, response, next) => {
    const isoDateNow = new Date().toISOString();
    const fullUrl = request.protocol + "://" + request.get("host") + request.originalUrl;
    const requestParamsString = Object.keys(request.params).length > 0 ? "/ Request params: " + JSON.stringify(request.params) : "";
    const requestDataString = request.body ? "/ Request body " + JSON.stringify(request.body) : "";
    response.on("finish", () => {
        console.info(`[${isoDateNow}] ${request.method} ${fullUrl} / ${response.statusCode} ${response.statusMessage} ${requestParamsString} ${requestDataString}`);
    });
    next();
};

app.use(logger);

// Save database on PUT, POST and DELETE requests
const saveDatabaseToFile = (request, response, next) => {
    if (["PUT","POST", "DELETE"].includes(request.method)) {
        response.on("finish", function() {
            saveDatabase();
        });
    }
    next();
};

app.use(saveDatabaseToFile);

// Login / register
app.use(authenticationRouter);

// Routes
app.use(apiPrefix, booksRouter);
app.use(apiPrefix, usersRouter);

// Start app
app.listen(port, () => {
    readDatabase();
    console.info(`Listening to port ${port}`);
});