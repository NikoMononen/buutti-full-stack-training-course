import { getMockReq, getMockRes } from "@jest-mock/express";
import { jest } from "@jest/globals";
import { users } from "../controllers/users.controller.js";
import { books } from "../controllers/books.controller.js";
import * as BooksController from "../controllers/books.controller.js";

// eslint-disable-next-line no-unused-vars
const { res, next, mockClear } = getMockRes();

// Data
const mockUsersDatabase = [
    {
        "id":"d115b98a-150f-46f6-b6e5-5fbb34f0e5a2",
        "username":"user",
        "password":"$2b$10$pIPZqL9UPuaYEKtJYMRYn.nWWXRb0u4h/jdFlHfBpmuduAkS8HxrK",
        "friends": ["6144c309-4b59-4542-81d0-22867862739a"]
    },
    {
        "id":"6144c309-4b59-4542-81d0-22867862739a",
        "username":"user2",
        "password":"$2b$10$kQuy47qkjfDaDAfdri6vVegSgBsLtpy.TqC9pd3OkeuoZX3Gs64hm",
        "friends":[]
    }
];
const mockBooksDatabase = [
    {
        "id":"ae7a4a9d-cdc2-47bf-b642-7a625d666d3c",
        "name":"Lord of the rings",
        "author":"J.R.R. Tolkien",
        "read":true,
        "owner":"d115b98a-150f-46f6-b6e5-5fbb34f0e5a2"
    },
    {
        "id":"1dfaccaa-46e7-4493-a24a-33fc4c9586ae",
        "name":"REST API Design Rulebook",
        "author":"Mark Masse",
        "read":false,
        "owner":"6144c309-4b59-4542-81d0-22867862739a"
    }
];

describe("Users controller tests' ", () => {
    beforeEach(async ()  => {
        jest.resetModules(); // Clear the cache
        mockClear();
        console.info = jest.fn();
        // Mock database
        mockUsersDatabase.forEach(user => users.push({ ...user }));
        mockBooksDatabase.forEach(book => books.push({ ...book }));
    });

    afterEach(async ()  => {
        users.splice(0, users.length);
        books.splice(0, books.length);
    });
      
    test("should 200 and return all books for authenticated user", async () => {
        const authenticatedUser = users[0];
        const req = getMockReq();

        req.authenticatedUser = authenticatedUser;
        await BooksController.getAllBooks(req, res);

        const userBooks = books.filter((book) => book.owner === authenticatedUser.id);
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.arrayContaining(userBooks));
    });

    test("should 200 and return book for given id", async () => {
        const authenticatedUser = users[0];
        const book = books.find(book => book.owner === authenticatedUser.id);
        const req = getMockReq({
            params: {
                "id": book.id
            },
            authenticatedUser: authenticatedUser
        });

        await BooksController.getBook(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.objectContaining(book));
    });

    test("should 403 and return all books for authenticated user", async () => {
        const authenticatedUser = users[0];
        const req = getMockReq({
            params: {
                "id": "notFound"
            },
            authenticatedUser: authenticatedUser
        });

        await BooksController.getBook(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ 
            error: "Book not found" 
        });
    });

    test("posting book should 200 and return created book", async () => {
        const authenticatedUser = users[0];
        const newBook = { 
            name: "Book name", 
            author: "Book author",
            read: false,
            owner : authenticatedUser.id
        };

        const req = getMockReq({
            authenticatedUser: authenticatedUser,
            body:  newBook
        });

        await BooksController.postBook(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.objectContaining({
            ...newBook,
            id: expect.any(String)
        }));
    });

    test("updating existing book should 200 and return book", async () => {
        const authenticatedUser = users[0];
        const book = books.find(book => book.owner === authenticatedUser.id);

        const req = getMockReq({
            authenticatedUser: authenticatedUser,
            params: {
                id: book.id
            },
            body: book
        });

        await BooksController.putBook(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(book);
    });

    test("updating book that authenticated user is not owner should 403 and return error message", async () => {
        const authenticatedUser = users[0];
        const book = books.find(book => book.owner !== authenticatedUser.id);

        const req = getMockReq({
            authenticatedUser: authenticatedUser,
            params: {
                id: book.id
            },
            body: book
        });

        await BooksController.putBook(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ 
            error: "Book not found" 
        });
    });

    test("updating not existing book should 403 and return error message", async () => {
        const authenticatedUser = users[0];
        const updateBook = { 
            id: "test-id", 
            name: "Book name", 
            author: "Book author",
            read: false,
            owner : authenticatedUser.id
        };

        const req = getMockReq({
            authenticatedUser: authenticatedUser,
            params: {
                id: "notFoundId"
            },
            body:  updateBook
        });

        await BooksController.putBook(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ 
            error: "Book not found" 
        });
    });

    test("should 200 and return all friends books for authenticated user", async () => {
        const authenticatedUser = users[0];
        const req = getMockReq();

        req.authenticatedUser = authenticatedUser;
        await BooksController.getAllFriendBooks(req, res);

        const userBooks = books.filter((book) => authenticatedUser.friends.includes(book.owner));
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.arrayContaining(userBooks));
    });

    test("should 200 and return all specified friend books for authenticated user", async () => {
        const authenticatedUser = users[0];
        const friend = users.find(user => user.id === authenticatedUser.friends[0]);
        const req = getMockReq({
            params: {
                id: friend.username
            },
            authenticatedUser: authenticatedUser
        });

        await BooksController.getFriendBooks(req, res);

        const friendBooks = books.filter((book) => book.owner === friend.id);
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.arrayContaining(friendBooks));
    });

    test("should 403 friend not in authenticated user friend list and return error message", async () => {
        const authenticatedUser = users[0];
        const req = getMockReq({
            params: {
                id: "notFoundUsername"
            },
            authenticatedUser: authenticatedUser
        });

        await BooksController.getFriendBooks(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ 
            error: "Friend not in user friend list" 
        });
    });

    test("should 200 when deleting authenticated userbook and return message", async () => {
        const authenticatedUser = users[0];
        const book = books.find(book => book.owner === authenticatedUser.id);
        const req = getMockReq({
            params: {
                id: book.id
            },
            authenticatedUser: authenticatedUser
        });

        await BooksController.deleteBook(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith();
    });

    test("should 403 when deleting book that does not exists and return error message", async () => {
        const authenticatedUser = users[0];
        const req = getMockReq({
            params: {
                id: "notFoundId"
            },
            authenticatedUser: authenticatedUser
        });

        await BooksController.deleteBook(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
            error: "Book not found"
        });
    });

    test("should 403 when deleting authenticated book that user is not owner and return error message", async () => {
        const authenticatedUser = users[0];
        const book = books.find(book => book.owner !== authenticatedUser.id);
        const req = getMockReq({
            params: {
                id: book.id
            },
            authenticatedUser: authenticatedUser
        });

        await BooksController.deleteBook(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
            error: "Book not found"
        });
    });
});