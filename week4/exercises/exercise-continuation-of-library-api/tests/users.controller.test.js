import { getMockReq, getMockRes } from "@jest-mock/express";
import { jest } from "@jest/globals";
import { users } from "../controllers/users.controller.js";
import * as UsersController  from "../controllers/users.controller.js";

// eslint-disable-next-line no-unused-vars
const { res, next, mockClear } = getMockRes();

// Data
const mockDatabase = [
    {
        "id":"d115b98a-150f-46f6-b6e5-5fbb34f0e5a2",
        "username":"user",
        "password":"$2b$10$pIPZqL9UPuaYEKtJYMRYn.nWWXRb0u4h/jdFlHfBpmuduAkS8HxrK",
        "friends": ["6144c309-4b59-4542-81d0-22867862739a"]
    },
    {
        "id":"6144c309-4b59-4542-81d0-22867862739a",
        "username":"user2",
        "password":"$2b$10$kQuy47qkjfDaDAfdri6vVegSgBsLtpy.TqC9pd3OkeuoZX3Gs64hm",
        "friends":[]
    }
];

describe("Users controller tests' ", () => {
    beforeEach(async ()  => {
        jest.resetModules(); // Clear the cache
        mockClear();
        console.info = jest.fn();
        // Mock database
        mockDatabase.forEach(user => users.push({ ...user, friends: [] }));
    });

    afterEach(async ()  => {
        users.splice(0,users.length);
    });
      
    test("should 200 and return all users", async () => {
        const req = getMockReq();

        await UsersController.getAllUsers(req, res);

        const userNames = users.map(user => user.username);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.arrayContaining(userNames));
    });


    test("should 200 and authenticated user friends empty", async () => {
        const req = getMockReq();
        const authenticatedUser = users.shift();
        req.authenticatedUser = authenticatedUser;
    
        await UsersController.getFriends(req, res);

        const authenticatedUserFriends = authenticatedUser.friends.map((userId) => {
            const user = users.find((user) => user.id === userId);
            if (user) return user.username;
        });

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.arrayContaining(authenticatedUserFriends));
    });

    test("should 200 and authenticated user friends", async () => {
        const authenticatedUser = users.shift();
        const newUser = {
            "id":"6144c309-4b59-4542-81d0-228678627393",
            "username":"newUser",
            "password":"$2b$10$kQuy47qkjfDaDAfdri6vVegSgBsLtpy.TqC9pd3OkeuoZX3Gs64hm",
            "friends":[]
        };
        users.push(newUser);
        authenticatedUser.friends.push(newUser.id);

        const req = getMockReq();
        req.authenticatedUser = authenticatedUser;
        await UsersController.getFriends(req, res);

        const authenticatedUserFriends = authenticatedUser.friends.map((userId) => {
            const user = users.find((user) => user.id === userId);
            if (user) return user.username;
        });

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.arrayContaining(authenticatedUserFriends));
    });

    test("should 200 and add friend for authenticated user", async () => {
        const authenticatedUser = users.shift();
        const newUser = {
            "id":"6144c309-4b59-4542-81d0-228678627393",
            "username":"newUser",
            "password":"$2b$10$kQuy47qkjfDaDAfdri6vVegSgBsLtpy.TqC9pd3OkeuoZX3Gs64hm",
            "friends":[]
        };
        users.push(newUser);

        const req = getMockReq({
            "body": {
                "username": "newUser"
            }
        });
        req.authenticatedUser = authenticatedUser;

        await UsersController.addFriend(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
            status: "User added to friends"
        });
        expect(authenticatedUser.friends).toStrictEqual([newUser.id]);
    });

    test("should 409 and return error message", async () => {
        const authenticatedUser = users.shift();
        const newUser = {
            "id":"6144c309-4b59-4542-81d0-228678627393",
            "username":"newUser",
            "password":"$2b$10$kQuy47qkjfDaDAfdri6vVegSgBsLtpy.TqC9pd3OkeuoZX3Gs64hm",
            "friends":[]
        };
        users.push(newUser);
        authenticatedUser.friends.push(newUser.id);

        const req = getMockReq({
            "body": {
                "username": "newUser"
            }
        });
        req.authenticatedUser = authenticatedUser;

        await UsersController.addFriend(req, res);

        expect(res.status).toHaveBeenCalledWith(409);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
            error: "User already in friends"
        });
        expect(authenticatedUser.friends).toStrictEqual([newUser.id]);
    });

    test("should 404 and return user not found", async () => {
        const authenticatedUser = users.shift();

        const req = getMockReq({
            "body": {
                "username": "userNotFound"
            }
        });
        req.authenticatedUser = authenticatedUser;

        await UsersController.addFriend(req, res);

        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
            error: "User not found"
        });
        expect(authenticatedUser.friends).toStrictEqual([]);
    });
});