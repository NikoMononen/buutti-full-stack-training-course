import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';

const initialState = [
  { 
    id: 1,
    title: "Clear todo", 
    complete: false
  },
];

const Todolist = function(props) {
  const [todoList, setTodoList] = useState(props.list);
  const [newTodo, setNewTodoText] = useState("");
  const [id, setId] = useState(2);

  const formSubmitHandler = (event) => {
    event.preventDefault();
    if(newTodo === "" || newTodo === undefined || newTodo === null) return;
    todoList.push({
      id: id,
      title: newTodo,
      complete: false
    });

    const newId = id + 1; 
    setId(newId);
    setTodoList([...todoList]);
  }

  const newTodoTextHandler = (event) => {
    setNewTodoText(event.target.value);
  }

  const toggleComplete = (event) => {
      const todoItem = todoList.find((todo) => parseInt(event.target.id) === todo.id);
      if(todoItem === -1) return;
      todoItem.complete = !todoItem.complete;
      setTodoList([...todoList]);
  }

  const removeTodo = (event) => {
      console.log(event.target);
      const todoItemIndex = todoList.findIndex((todo) => parseInt(event.target.parentNode.id) === todo.id);
      if(todoItemIndex === -1) return;
      todoList.splice(todoItemIndex,1);
      setTodoList([...todoList]);
  }

  const list = todoList.map((todo) => {
    const isCompleted = todo.complete ? "completed" : null;
    return <li 
      key={todo.id} 
      id={todo.id}
      className={isCompleted}
      onClick={toggleComplete}>{todo.title} <button onClick={removeTodo}>X</button> 
      </li>
  });

  return (
    <div>
      <h1>Todo list</h1>
      <ul>{ list }</ul>
      <hr/>
      <form onSubmit={formSubmitHandler}>
        <input type="text" name="todo"  value={newTodo} onChange={newTodoTextHandler} />
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

function App(props) {
  return (
    <Todolist list={initialState}/>
  );
}

export default App;

