import React, { useState, useEffect } from 'react';

const BookEdit = ({book, addNewBook, saveChanges, deleteBook})=> {
  const [bookName, setBookName] = useState(book.name); 
  const [bookAuthor, setBookAuthor] = useState(book.author);
  const [bookRead, setBookRead] = useState(book.read);

  useEffect(() => {
    setBookName(book.name)
    setBookAuthor(book.author)
    setBookRead(book.read)
  }, [book]);

  const formSubmitHandler = (event) => {
    event.preventDefault();
    saveChanges(book.id, { name: bookName, author: bookAuthor, read: bookRead });
  }

  const addBookHandler = (event) => {
    event.preventDefault();
    const newBook = { name: bookName, author: bookAuthor, read: bookRead };
    addNewBook(newBook);
  }

  const deleteBookHandler = (event) => {
    event.preventDefault();
    deleteBook(book.id);
  }

  const radioButtonHandler = (event) => {
    setBookRead(event.target.value === "true" ? true : false)
  }

  // Form
  return (
    <>
    <form onSubmit={formSubmitHandler}>
      <div class="mb-2">
        <label htmlFor="name">Name
          <input class="form-control" type="text" id="name" value={bookName} onChange={(e) => setBookName(e.target.value)}/>
        </label>
      </div>

      <div class="mb-2">
        <label class="form-label" htmlFor="author">Author
          <input class="form-control" type="text" id="author" value={bookAuthor} onChange={(e) => setBookAuthor(e.target.value)} />
        </label>
      </div>
      <div class="mb-2">
        <div class="mb-2">
          Read
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" id="read" name="isRead" value="true" checked={bookRead === true}  onChange={radioButtonHandler} />
          <label class="form-check-label" htmlFor="read">Yes</label>
        </div>

        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" id="notRead" name="isRead" value="false" checked={bookRead === false} onChange={radioButtonHandler} />
          <label class="form-check-label" htmlFor="notRead">No</label>
        </div>
      </div>
      <div class="mb-2">
        <div class="btn-group" role="group">
          <button type="submit" class="btn btn-success">Save</button> 
          <button class="btn btn-secondary" onClick={addBookHandler}>Create</button>
          <button class="btn btn-danger" onClick={deleteBookHandler}> Delete</button>
        </div>
      </div>
    </form>
    </>
  );
};

export default BookEdit;
