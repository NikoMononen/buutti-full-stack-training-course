import React from "react";
import Book from './book.js';

const BookList = function({books, setSelectedBook}) {

  const renderBooks = () => books.map((book) => 
    <Book 
        key={book.id}
        book={book}
        onClick={(e) => setSelectedBook(book)}
    />
  );

  return (
    <div class="books">
      {renderBooks()}
    </div>
  );
}

export default BookList;
