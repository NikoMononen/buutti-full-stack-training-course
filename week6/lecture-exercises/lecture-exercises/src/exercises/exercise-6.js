import React, { useState, useEffect } from 'react';

const Timer = () => {
  const [seconds, setSeconds] = useState(0);

  function reset() {
    setSeconds(0);
  }

  useEffect(() => {
    const interval = setTimeout(() => setSeconds(seconds + 1), 1000);
    return () => clearInterval(interval);
  }, [seconds]);

  return (
    <div className="app">
      <div className="time">
        {` ${new Date(seconds * 1000, ).toLocaleTimeString()} `}
      </div>
    </div>
  );
};


function App() {
  return (
    <div className="App">
      <Timer />
    </div>
  );
}

export default App;
