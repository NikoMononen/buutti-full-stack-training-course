import React, { useState } from 'react';

 const randomNumber = () => {
   return 1;
 }

 const roll = () => {
  return randomNumber();
}

 const initialState = [];

 const Bingo = () => {
  const [state, setState] = useState( initialState );
  return (
    <div>
      <h1>Bingo</h1>
      <button onClick={() => setState(state.push(randomNumber)) }>Roll</button>
      <BingoNumbers />
    </div>
  );
 };

const BingoNumbers = () => {
  const [state, setState ] = useState(initialState)
  const listOfBingoNumbers = state.map(number => {
    return number;
  });
  return (
    <div>{ listOfBingoNumbers }</div>
  );
 };

function App() {
  return (
    <div className="App">
      <Bingo />
    </div>
  );
}

export default App;
