import mongoose from "mongoose";

const Schema = mongoose.Schema, ObjectId = Schema.ObjectId;
const CarsSchema = new mongoose.Schema({
    id: Number,
    owner_id: ObjectId,
    car_make: String,
    car_model: String,
    car_modelyear: Number,
    price: Number,
});

export const Cars = mongoose.models.cars || mongoose.model("cars", CarsSchema);