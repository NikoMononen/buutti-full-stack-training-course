import express from "express";
import * as UsedCarsController from "../controllers/usedCars.controller.js";

const router = express.Router();

router.get("/:id", UsedCarsController.getUsedCar);
router.post("/", UsedCarsController.postUsedCar);

export default router;