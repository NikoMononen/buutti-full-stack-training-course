import express from "express";
import * as BankController from "../controllers/bank.controller.js";
import * as UserController from "../controllers/user.controller.js";
import { authorizationMiddleware } from "../controllers/authentication.controller.js";

const router = express.Router();

// Open access
router.post("/bank/user", UserController.createUser);
router.get("/bank/:user_id/balance", BankController.getBalance);

// Use JWT authentication middleware
router.use(authorizationMiddleware);

// Bank actions
router.patch("/bank/user/withdraw", BankController.withdraw);
router.patch("/bank/user/deposit", BankController.deposit);
router.post("/bank/transfer", BankController.transfer);

router.get("/bank/request_funds", BankController.getFundRequests);
router.post("/bank/request_funds", BankController.createFundRequest);
router.patch("/bank/request_funds", BankController.acceptFundRequest);

// User actions
router.patch("/bank/update", UserController.updateUsername);
router.patch("/bank/user/password", UserController.updatePassword);

export default router;