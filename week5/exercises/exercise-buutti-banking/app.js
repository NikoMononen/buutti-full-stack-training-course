import express from "express";
import authenticationRouter from "./routes/authentication.route.js";
import bankRouter from "./routes/bank.route.js";
import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();
const PORT = 5000;

// Try to connect mongodb
try {
    mongoose.connect(
        "mongodb://127.0.0.1:27017/testDatabase",
        { 
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            user: process.env.MONGODB_USER,
            pass: process.env.MONGODB_PASSWORD
        }
    );
}
catch(error) {
    console.log(error);
    process.exit(1);
}

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Logger middleware
const logger = (request, response, next) => {
    const isoDateNow = new Date().toISOString();
    const fullUrl = request.protocol + "://" + request.get("host") + request.originalUrl;
    const requestParamsString = Object.keys(request.params).length > 0 ? "/ Request params: " + JSON.stringify(request.params) : "";
    const requestDataString = request.body ? "/ Request body " + JSON.stringify(request.body) : "";
    response.on("finish", () => {
        console.info(`[${isoDateNow}] ${request.method} ${fullUrl} / ${response.statusCode} ${response.statusMessage} ${requestParamsString} ${requestDataString}`);
    });
    next();
};

// Use request logging
app.use(logger);

// Login 
app.use(authenticationRouter);

// Routes
app.use(bankRouter);

// Start app
app.listen(PORT, () => {
    console.log(`Listening to ${PORT}`);
});