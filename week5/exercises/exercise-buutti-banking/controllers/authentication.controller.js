import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { User } from "../models/user.model.js";

/**
 * Authorization middleware. Validates given JWT token and
 * stores authenticated user to req.authenticatedUser 
 */ 
export const authorizationMiddleware = async (request, response, next) => {
    const authToken = request.get("authorization");
    if (!authToken || !authToken.startsWith("Bearer ")) {
        response.status(401).json({
            error: "Auth token missing"
        });
        return;
    }
    try {
        // Substring "Bearer " and verify token
        const token = authToken.substring(7);
        const decodedToken = jwt.verify(token, process.env.APP_SECRET);

        if (!decodedToken._id) {
            return response.status(403).json({
                error: "Access token is invalid"
            });
        } else {
            request.authenticatedUser = await User.findById(decodedToken._id);
            next();
        }
    }
    catch (error) {
        switch (error.constructor) {
            case jwt.TokenExpiredError: {
                return response.status(403).json({
                    error: error.message
                });
            }
            case jwt.JsonWebTokenError: {
                return response.status(401).json({
                    error: error.message
                });
            }
            default: {
                return response.status(500).json({
                    error: error.message
                });
            }
        }
    }
};

/**
 * Login. Creates and returns JWT token on successful login
 */
export const login = async (request, response) => {
    const userId = request.body.id;
    const userPassword = request.body.password;
    const user = await User.findById(userId);

    if (user === null) {
        response.status(401).json({
            "status": "Access denied!"
        });
        return;
    }
    try {
        const passwordIsCorrect = await bcrypt.compare(userPassword, user.password);
        if (!passwordIsCorrect) {
            response.status(403).json({
                "status": "Access denied!"
            });
            return;
        }
        // Sign JWT token
        try {
            const token = jwt.sign(
                { _id: user._id },
                process.env.APP_SECRET,
                { expiresIn: "8h" }
            );
            response.status(200).json({
                "status": "Access granted!",
                "token": token
            });
        } catch (error) {
            // jwt.sign thrown error
            response.status(500).json({
                "error": error.message
            });
        } 
    }
    catch (error) {
        // bcrypt.compare thrown error
        response.status(500).json({
            "error": error.message
        });
    }
};