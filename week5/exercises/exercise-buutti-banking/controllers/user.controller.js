import { User } from "../models/user.model.js";

/**
 *  Creates new user
 */
export const createUser = async (req, res) => {
    const validationErrors = [];
    const userName = req.body.name;
    const userPassword = req.body.password;
    const userBalance  = Number(req.body.deposit);

    if (userName === undefined || userName.length < 1) {
        validationErrors.push("Name must be given");
    }

    if (userPassword === undefined || userPassword.length < 1) {
        validationErrors.push("Password must be given");
    }

    if (isNaN(userBalance) || userBalance < 0) {
        validationErrors.push("Deposit must be positive number");
    }

    if (validationErrors.length > 0 ) {
        res.status(400).json({ 
            error: validationErrors
        });
        return;
    }

    const user = await User.find({ name: "userName" });

    if (user !== null) {
        res.status(409).json({ 
            error: "Username already exists"
        });
        return;
    }
    
    const newUser = new User({
        name: userName,
        password: userPassword,
        balance: userBalance
    });

    newUser.save().then(result => {
        res.status(200).json({ id: result.id });
    }).catch(error => {
        res.status(500).json(error);
    });
};

/**
 *  Updates authenticated user name
 */
export const updateUsername = async (req, res) => {
    const validationErrors = [];
    const userId = req.authenticatedUser.id;
    const userName = req.body.name;

    if (userName === undefined || userName.length < 1) {
        validationErrors.push("Name must be given");
    }

    if (validationErrors.length > 0 ) {
        res.status(400).json({ 
            error: validationErrors
        });
        return;
    }
    
    const existingUser = await User.find({ name: "userName" });

    if (existingUser) {
        res.status(409).json({ 
            error: "Username already exists"
        });
        return;
    }

    const user = await User.findById(userId);

    if (user) {
        user.name = userName;
        user.save().then(result => {
            res.status(200).json({ name: result.name });
        }).catch(error => {
            res.status(500).json(error);
        });
    } 
    else {
        res.status(404).json({ 
            error: "User not found"
        });
    }
};

/**
 *  Updates authenticated user password
 */
export const updatePassword = async (req, res) => {
    const validationErrors = [];
    const userId = req.authenticatedUser.id;
    const userPassword = req.body.password;

    if (userId === undefined || userId.length < 1) {
        validationErrors.push("Id must be given");
    }

    if (userPassword === undefined || userPassword.length < 1) {
        validationErrors.push("Password must be given");
    }

    if (validationErrors.length > 0 ) {
        res.status(400).json({ 
            error: validationErrors
        });
        return;
    }

    const user = await User.findById(userId);

    if (user) {
        user.password = userPassword;
        user.save().then(result => {
            res.status(200).json({ password: result.password });
        }).catch(error => {
            res.status(500).json(error);
        });
    } 
    else {
        res.status(404).json({ 
            error: "User not found"
        });
    }
};



