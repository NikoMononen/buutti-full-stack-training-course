import express from "express";
import * as StudentController from "../controllers/students.controller.js";

const router = express.Router();

router.get("/", StudentController.getAllStudents);
router.get("/:id", StudentController.getStudent);
router.post("/", StudentController.postStudent);
router.put("/:id", StudentController.putStudent);
router.delete("/:id", StudentController.deleteStudent);

export default router;