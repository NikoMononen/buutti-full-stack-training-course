import uuid4 from "uuid4";
import { Students } from "../models/students.model.js";

export const getAllStudents = (req, res) => {
    Students.find().then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};

export const getStudent = (req, res) => {
    Students.find({uuid: req.params.id}).then(result => {
        if (result.length > 0) res.status(200).json(result);
        else res.status(404).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};

export const postStudent = (req, res) => {
    const newStudent = new Students({...req.body, uuid: uuid4()});
    newStudent.save().then(result => {
        if (result.length > 0) res.status(200).json(result);
        else res.status(404).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};

export const putStudent = (req, res) => {
    Students.findOneAndUpdate({uuid: req.params.id}, req.body).then(result => {
        if (result.length > 0) res.status(200).json(result);
        else res.status(404).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};

export const deleteStudent = (req, res) => {
    Students.findOneAndDelete({uuid: req.params.id}).then(result => {
        if (result.length > 0) res.status(200).json(result);
        else res.status(404).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};
