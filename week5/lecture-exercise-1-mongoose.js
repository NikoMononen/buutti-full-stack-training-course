import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();

async function connectMongoose() {
    await mongoose.connect(
        "mongodb://127.0.0.1:27017/testDatabase",
        { 
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            user: process.env.MONGODB_USER,
            pass:  process.env.MONGODB_PASSWORD
        }
    );
}
connectMongoose();

const UserSchema = new mongoose.Schema({
    name: String,
    favouriteColor: String,
});

const User = mongoose.model("Students", UserSchema);
const user = new User({
    name: "Seppo",
    favouriteColor: "Blue"
});

const result = await user.save();
console.log(result);

const users = await User.find();
users.forEach(user => console.log(user));

const removedUser = await User.findOneAndDelete({
    name: "Seppo"
});
console.log(removedUser);

mongoose.connection.close();

