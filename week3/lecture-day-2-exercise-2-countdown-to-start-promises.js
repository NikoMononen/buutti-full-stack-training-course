"use strict";
/*
function countDown(msg) {
    return new Promise((resolve) => {
        setTimeout(() => {
            console.log(msg);
            resolve();
        },1000);
    });
}

countDown("3").then(()=> {
    countDown("..2").then(()=> {
        countDown("....1").then(()=> {
            countDown("GO!");
        });
    });
});

----

function print(txt) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log(txt);
            resolve();
        }, 1000);
    });
}

print(3)
.then(() => print(2))
.then(() => print(1))
.then(() => print("GO"));

--- 

function print(txt) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log(txt);
            resolve();
        }, 1000);
    });
}

async function countdown() {
    await print(3);
    await print(2);
    await print(1);
    await print("GO");
}

countdown();
*/
function countDown(n) {
    return new Promise(resolve => {
        console.log(n--);
        if (n > 0) {
            setTimeout(() => {
                countDown(n).then(resolve);
            }, 1000);
        } else {
            resolve();
        }
    });
}

countDown(3).then(() => {
    console.log("GO!");
});