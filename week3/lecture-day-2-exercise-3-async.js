const getValue = function () {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve({ value: Math.random() });
        }, Math.random() * 1500);
    });
};
// With async/await
const valueOneHere = await getValue();
const valueTwoHere = await getValue();
console.log(`Value 1 is ${valueOneHere.value} and value 2 is ${valueTwoHere.value}`);

// With promises
getValue().then((resultObject) => {
    const valueOneHere = resultObject;
    getValue().then((resultObject) => {
        const valueTwoHere = resultObject;
        console.log(`Value 1 is ${valueOneHere.value} and value 2 is ${valueTwoHere.value}`);
    });
});

// Multiple promises
Promise.all([getValue(), getValue()])
    .then(values => {
        console.log(`Value 1 is ${values[0].value} and value 2 is ${values[1].value}`);
    });